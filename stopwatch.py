#!/usr/bin/env python3

# File: timer.py
# Author: Wayne Lewis
# Date: 2018-04-03
#
# Description:
# Simple elapsed timer display

import curses
import time
import datetime

def timer(stdscr):
    # Make stdscr.getch() non-blocking
    stdscr.nodelay(True)
    k = 0
    color = 7
    MAX_COLORS = 7
    stdscr.clear()
    stdscr.refresh()
    running = False
    elapsed_time = datetime.timedelta(0,0,0)

    # Set up colors
    curses.curs_set(0)
    curses.curs_set(0)
    curses.curs_set(0)
    curses.start_color()
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(4, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(5, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
    curses.init_pair(6, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(7, curses.COLOR_WHITE, curses.COLOR_BLACK)

    # Initialize display
    out_str = "{:02d}:{:02d}:{:02d}.{:03d}".format(0,0,0,0)
    stdscr.addstr(0, 0, out_str, curses.color_pair(color))
    stdscr.refresh()


    # Main loop
    while (k != ord('q')):
        # Test for keypress
        if (k == ord('r')):
            elapsed_time = datetime.timedelta(0,0,0)
            if not running:
                out_str = "{:02d}:{:02d}:{:02d}.{:03d}".format(0,0,0,0)
                stdscr.addstr(0, 0, out_str, curses.color_pair(color))
                stdscr.refresh()
        elif (k == ord('s')):
            last_time = datetime.datetime.now()
            running = True
        elif (k == ord('p')):
            running = False
        elif (k == ord(' ')):
            running = not running
            if running:
                last_time = datetime.datetime.now()
        elif (k == ord('c')):
            color += 1
            if color > MAX_COLORS:
                color = 1
            if not running:
                stdscr.addstr(0, 0, out_str, curses.color_pair(color))
                stdscr.refresh()

        if running:
            current_time = datetime.datetime.now()
            elapsed_time += current_time - last_time
            last_time = current_time
            hours = elapsed_time.seconds // 3600
            minutes = (elapsed_time.seconds % 3600) // 60
            seconds = elapsed_time.seconds % 60
            milliseconds = elapsed_time.microseconds // 1000
            out_str = "{:02d}:{:02d}:{:02d}.{:03d}".format(hours, minutes, seconds, milliseconds)

            stdscr.addstr(0, 0, out_str, curses.color_pair(color))
            stdscr.refresh()

        # Get keypress
        k = stdscr.getch()
        # Sleep so we don't hog resources
        time.sleep(0.02)

def main():
    curses.wrapper(timer)

if __name__ == "__main__":
    main()
