# Stopwatch

Basic curses stopwatch application.

## Instructions

* s - start
* p - stop
* Space - start/stop toggle
* r - reset
* q - quit
* c - change color
